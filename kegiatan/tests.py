from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,add,tampilkan
from .models import ToDo, Person
from .forms import FormToDo
from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
class testkegiatan(TestCase):
    def test_lab_5_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_add_url(self):
        response = Client().get('/add/')
        self.assertEqual(response.status_code, 200)
    def test_template(self):
        response = Client().get('/add/')
        self.assertTemplateUsed(response, 'add.html')
    def test_func_2(self):
        found = resolve('/add/')
        self.assertEqual(found.func, add)
    def test_tampilkan_url(self):
        response = Client().get('/tampilkan/')
        self.assertEqual(response.status_code, 200)
    def test__tampilkan_func(self):
        found = resolve('/tampilkan/')
        self.assertEqual(found.func, tampilkan)
        
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = FormToDo()
        self.assertIn('id="id_title"', form.as_p())
        self.assertIn('input type="text"', form.as_p())

    def test_lab5_post_success_and_render_the_result(self):
            test = 'ppw'
            response_post = Client().post('/add/', {'title': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/tampilkan/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

    def test_person(self):
            test = 'ppw'
            response_post = Client().post('/addorang/', {'title': test})
    
    def test_error(self):
            test = 'ppw'
            response_post = Client().post('/add/', {'title': ''})
            self.assertEqual(response_post.status_code, 200)
            response= Client().get('/tampilkan/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)

    def test_hapus(self):
        obj = ToDo.objects.create(title="ppw")
        response_post = Client().post('/hapus/1')
        jumlah = ToDo.objects.all().count()
        self.assertEqual(jumlah,0)

    def test_addorang(self):
        test = 'ppw'
        obj = ToDo.objects.create(title="ppw")
        response_post = Client().post('/addorang/1', {'nama': test})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/tampilkan/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        
    def test_errororang(self):
        test = 'abc'
        obj = ToDo.objects.create(title="ppw")
        response_post = Client().post('/addorang/1', {'nama': ''})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/tampilkan/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

 

    



    

