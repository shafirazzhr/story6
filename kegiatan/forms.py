from django import forms
from .models import Person,ToDo

class FormOrang(forms.Form):
    nama = forms.CharField(max_length=30)

class FormToDo(forms.ModelForm):
    class Meta:
        model = ToDo
        fields = '__all__'
    error_messages = {
        'required':'Please Type'
    }
