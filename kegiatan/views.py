from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import FormOrang,FormToDo
from .models import ToDo, Person

# Create your views here.
response = {}
def index(request):
    return render(request, 'kegiatan.html')
    
def add(request):
    if request.method == "POST":
        form = FormToDo(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/tampilkan')
        else:
            return HttpResponse("Data tidak valid")
    form = FormToDo()
    return render(request,'add.html',{'form':form})

def tampilkan(request):
	todo = ToDo.objects.all()
	response['ToDo'] = todo
	html = 'tampilkan.html'
	return render(request, html, response)

def satu(request,id):
    todo = ToDo.objects.get(id=id)
    orang = Person.objects.filter(kegiatan=todo)
    response['kegiatan'] = todo
    response['orang'] = orang
    html = 'satu.html'
    return render(request, html, response)

def hapus(request,id):
	kegiatan = ToDo.objects.get(id=id)
	kegiatan.delete()
	return HttpResponseRedirect('/tampilkan')

def addorang(request,id):
    kegiatan = ToDo.objects.get(id=id)
    if request.method == "POST":
        form = FormOrang(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            orang = Person.objects.create(kegiatan=kegiatan,nama=nama)
            return HttpResponseRedirect('/tampilkan')
        else:
            return HttpResponse("Data tidak valid")
    form = FormOrang()
    return render(request,'add.html',{'form':form})










