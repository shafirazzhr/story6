"""storyenam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from kegiatan import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('kegiatan.urls')),
    path('add/', views.add),
    path('tampilkan/',views.tampilkan),
    path('satu/<int:id>',views.satu, name='satu'),
    path('hapus/<int:id>',views.hapus, name='hapus'),
    path('addorang/<int:id>', views.addorang, name='addorang'),


]
